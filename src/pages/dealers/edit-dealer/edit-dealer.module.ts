import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EditDealerPage } from './edit-dealer';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [
    EditDealerPage,
  ],
  imports: [
    IonicPageModule.forChild(EditDealerPage),
    TranslateModule.forChild()
  ],
})
export class EditDealerPageModule {}
